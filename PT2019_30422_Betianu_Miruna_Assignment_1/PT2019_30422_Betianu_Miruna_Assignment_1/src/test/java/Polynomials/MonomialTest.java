package Polynomials;

import org.junit.Assert;

public class MonomialTest {

    @org.junit.Test
    public void setCoefficient() {
        Monomial m = new Monomial(1,2);
        m.setCoefficient(2);
        Assert.assertEquals(2,m.getCoefficient(),0.0);
    }

    @org.junit.Test
    public void getCoefficient() {
        Monomial m = new Monomial(1,2);
        Assert.assertEquals(1,m.getCoefficient(),0.0);
    }

    @org.junit.Test
    public void setExponent() {
        Monomial m = new Monomial(1,2);
        m.setExponent(1);
        Assert.assertEquals(1,m.getExponent(),0.0);
    }

    @org.junit.Test
    public void getExponent() {
        Monomial m = new Monomial(1,2);
        Assert.assertEquals(2,m.getExponent(),0.0);
    }
}