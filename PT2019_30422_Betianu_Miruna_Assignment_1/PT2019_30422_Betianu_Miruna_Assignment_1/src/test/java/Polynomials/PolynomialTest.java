package Polynomials;

import org.junit.Assert;
import org.junit.Test;

public class PolynomialTest {

    @Test
    public void addToPolynomial() {
        Polynomial p = new Polynomial(0,0);
        p.addToPolynomial(new Monomial(1,1));
        Assert.assertEquals(1,p.termNumber);
        Assert.assertEquals(1,p.degree);
        Assert.assertFalse("It doesn't work",p.myPolynomial.contains(new Monomial(1,1)));
    }

    @Test
    public void sortPolynomial() {
        Polynomial p = new Polynomial(0,0);
        Monomial a = new Monomial(1,1);
        Monomial b = new Monomial(2,5);
        Monomial c = new Monomial(5,4);
        p.addToPolynomial(a);
        p.addToPolynomial(b);
        p.addToPolynomial(c);
        p.sortPolynomial();

        Assert.assertEquals(a,p.getMonom(2));
        Assert.assertEquals(b,p.getMonom(0));
        Assert.assertEquals(c,p.getMonom(1));

    }

    @Test
    public void getMonom() {
        Monomial m = new Monomial(1,2);
        Polynomial p = new Polynomial(0,0);
        p.addToPolynomial(m);
        Assert.assertEquals(m,p.getMonom(0));
    }

    @Test
    public void deleteMonom() {
        Monomial m = new Monomial(1,2);
        Polynomial p = new Polynomial(0,0);
        p.addToPolynomial(m);
        p.deleteMonom(0);
        Assert.assertTrue("It hasn't been deleted",p.myPolynomial.contains(m));
    }

    @Test
    public void clearPolynomial() {
        Monomial m = new Monomial(1,2);
        Polynomial p = new Polynomial(0,0);
        p.addToPolynomial(m);
        p.clearPolynomial();
        Assert.assertTrue("It hasn't been cleared",p.myPolynomial.contains(m));
    }

    @Test
    public void noMonom() {
        Monomial m = new Monomial(1,2);
        Polynomial p = new Polynomial(0,0);
        p.addToPolynomial(m);
        p.noMonom();
        Assert.assertFalse("It has at least one monom",p.myPolynomial.contains(m));
    }
}