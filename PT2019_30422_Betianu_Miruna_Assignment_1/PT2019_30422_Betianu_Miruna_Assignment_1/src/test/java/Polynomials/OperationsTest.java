package Polynomials;

import org.junit.Assert;
import org.junit.Test;

public class OperationsTest {

    @Test
    public void sum() {
        Polynomial p = new Polynomial(0,0);
        p.addToPolynomial(new Monomial(1,1));
        p.addToPolynomial(new Monomial(2,2));
        p.addToPolynomial(new Monomial(1,3));

        Polynomial q = new Polynomial(0,0);
        q.addToPolynomial(new Monomial(1,2));
        q.addToPolynomial(new Monomial(3,1));

        Polynomial result_test = Operations.sum(p,q);
        Polynomial result = new Polynomial(0,0);
        result.addToPolynomial(new Monomial(1,3));
        result.addToPolynomial(new Monomial(3,2));
        result.addToPolynomial(new Monomial(4,1));

        Assert.assertEquals(result.getMonom(0).getExponent(),result_test.getMonom(0).getExponent());
        Assert.assertEquals(result.getMonom(0).getCoefficient(),result_test.getMonom(0).getCoefficient(),0.0);

        Assert.assertEquals(result.getMonom(1).getExponent(),result_test.getMonom(1).getExponent());
        Assert.assertEquals(result.getMonom(1).getCoefficient(),result_test.getMonom(1).getCoefficient(),0.0);

        Assert.assertEquals(result.getMonom(2).getExponent(),result_test.getMonom(2).getExponent());
        Assert.assertEquals(result.getMonom(2).getCoefficient(),result_test.getMonom(2).getCoefficient(),0.0);
    }

    @Test
    public void subtract() {
        Polynomial p = new Polynomial(0,0);
        p.addToPolynomial(new Monomial(1,1));
        p.addToPolynomial(new Monomial(2,2));
        p.addToPolynomial(new Monomial(1,3));

        Polynomial q = new Polynomial(0,0);
        q.addToPolynomial(new Monomial(1,2));
        q.addToPolynomial(new Monomial(3,1));

        Polynomial result_test = Operations.subtract(p,q);
        Polynomial result = new Polynomial(0,0);
        result.addToPolynomial(new Monomial(1,3));
        result.addToPolynomial(new Monomial(1,2));
        result.addToPolynomial(new Monomial(-2,1));

        Assert.assertEquals(result.getMonom(0).getExponent(),result_test.getMonom(0).getExponent());
        Assert.assertEquals(result.getMonom(0).getCoefficient(),result_test.getMonom(0).getCoefficient(),0.0);

        Assert.assertEquals(result.getMonom(1).getExponent(),result_test.getMonom(1).getExponent());
        Assert.assertEquals(result.getMonom(1).getCoefficient(),result_test.getMonom(1).getCoefficient(),0.0);

        Assert.assertEquals(result.getMonom(2).getExponent(),result_test.getMonom(2).getExponent());
        Assert.assertEquals(result.getMonom(2).getCoefficient(),result_test.getMonom(2).getCoefficient(),0.0);
    }

    @Test
    public void multiply() {
        Polynomial p = new Polynomial(0,0);
        p.addToPolynomial(new Monomial(1,1));
        p.addToPolynomial(new Monomial(2,2));
        p.addToPolynomial(new Monomial(1,3));

        Polynomial q = new Polynomial(0,0);
        q.addToPolynomial(new Monomial(1,2));

        Polynomial result_test = Operations.multiply(p,q);
        Polynomial result = new Polynomial(0,0);
        result.addToPolynomial(new Monomial(1,5));
        result.addToPolynomial(new Monomial(2,4));
        result.addToPolynomial(new Monomial(1,3));

        Assert.assertEquals(result.getMonom(0).getExponent(),result_test.getMonom(0).getExponent());
        Assert.assertEquals(result.getMonom(0).getCoefficient(),result_test.getMonom(0).getCoefficient(),0.0);

        Assert.assertEquals(result.getMonom(1).getExponent(),result_test.getMonom(1).getExponent());
        Assert.assertEquals(result.getMonom(1).getCoefficient(),result_test.getMonom(1).getCoefficient(),0.0);

        Assert.assertEquals(result.getMonom(2).getExponent(),result_test.getMonom(2).getExponent());
        Assert.assertEquals(result.getMonom(2).getCoefficient(),result_test.getMonom(2).getCoefficient(),0.0);
    }

    @Test
    public void division() {
        Polynomial p = new Polynomial(0,0);
        p.addToPolynomial(new Monomial(2,2));

        Polynomial q = new Polynomial(0,0);
        q.addToPolynomial(new Monomial(1,1));

        String result = Operations.division(p,q);
        Assert.assertEquals("Q:2x^1; R:0",result);

    }

    @Test
    public void derivative() {
        Polynomial p = new Polynomial(0,0);
        p.addToPolynomial(new Monomial(2,2));
        p.addToPolynomial(new Monomial(1,3));

        Polynomial result = Operations.derivative(p);

        Assert.assertEquals((new Monomial(3,2).getExponent()),result.getMonom(0).getExponent());
        Assert.assertEquals((new Monomial(3,2)).getCoefficient(),result.getMonom(0).getCoefficient(),0.0);

        Assert.assertEquals((new Monomial(4,1)).getExponent(),result.getMonom(1).getExponent());
        Assert.assertEquals((new Monomial(4,1)).getCoefficient(),result.getMonom(1).getCoefficient(),0.0);
    }

    @Test
    public void integrate() {
        Polynomial p = new Polynomial(0,0);
        p.addToPolynomial(new Monomial(6,5));
        p.addToPolynomial(new Monomial(1,3));

        Polynomial result = Operations.integrate(p);

        Assert.assertEquals((new Monomial(1,6).getExponent()),result.getMonom(0).getExponent());
        Assert.assertEquals((new Monomial(1,6)).getCoefficient(),result.getMonom(0).getCoefficient(),0.0);

        Assert.assertEquals((new Monomial(0.25,4)).getExponent(),result.getMonom(1).getExponent());
        Assert.assertEquals((new Monomial(0.25,4)).getCoefficient(),result.getMonom(1).getCoefficient(),0.0);

    }
}