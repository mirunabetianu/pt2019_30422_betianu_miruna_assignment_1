package Polynomials;

import java.util.ArrayList;
import java.util.Collections;

public class Polynomial {

    public ArrayList<Monomial> myPolynomial;
    public int termNumber;
    public int degree;

    public Polynomial(int termNumber, int degree)
    {
        this.myPolynomial = new ArrayList<Monomial>();
        this.termNumber = termNumber;
        this.degree = degree;
    }


    public void addToPolynomial(Monomial monomial)
    {

        myPolynomial.add(monomial);
        if(monomial.getExponent() > this.degree) this.degree = monomial.getExponent();
        termNumber++;

    }

    public void sortPolynomial()
    {
        DegreeComparator degreeComparator = new DegreeComparator();
        Collections.sort(myPolynomial,degreeComparator);
    }

    public Monomial getMonom(int i)
    {
        return this.myPolynomial.get(i);
    }

    public void deleteMonom(int i)
    {
        myPolynomial.remove(i);
    }

    public void clearPolynomial()
    {
        myPolynomial.clear();
        termNumber = 0;
        degree = 0;
    }

    public boolean noMonom()
    {
        return myPolynomial.isEmpty();
    }


}
