package Polynomials;
import javax.swing.*;
import java.text.DecimalFormat;
import java.util.Collections;

public class Operations {

    private static Polynomial compression(Polynomial p)
    {
        int degree = p.degree;

        int[] v = new int[degree+1];

        for(Monomial m : p.myPolynomial)
            v[m.getExponent()] += m.getCoefficient();

        Polynomial result = new Polynomial(0,degree);

        for(int i=0; i<=degree;i++)
            if(v[i] != 0)
            {
                result.myPolynomial.add(new Monomial(v[i],i));
                result.termNumber++;
            }
        result.sortPolynomial();
         if(!result.noMonom())result.degree = result.getMonom(0).getExponent();
         else result.degree = 0;

        return result;
    }

    public static Polynomial sum(Polynomial p1, Polynomial p2)
    {
        int degree;
        if(p1.degree < p2.degree) degree = p2.degree;
        else degree = p1.degree;

        Polynomial result = new Polynomial(0,degree);
        for(Monomial m : p1.myPolynomial)
        {
            result.myPolynomial.add(m);
            result.termNumber++;
        }
        for(Monomial m : p2.myPolynomial)
        {
            result.myPolynomial.add(m);
            result.termNumber++;
        }

        return compression(result);
    }

    public static Polynomial subtract(Polynomial p1, Polynomial p2)
    {
        for(Monomial m: p2.myPolynomial)
        {
            m.setCoefficient(m.getCoefficient()*(-1));
        }
        return sum(p1,p2);
    }

    public static Polynomial multiply(Polynomial p1, Polynomial p2)
    {
        Polynomial result = new Polynomial(0,p1.degree+p2.degree);

        for(Monomial m: p1.myPolynomial)
        {
            for(Monomial n: p2.myPolynomial)
            {
                Monomial monomial_r = new Monomial(m.getCoefficient()*n.getCoefficient(),m.getExponent()+n.getExponent());
                result.myPolynomial.add(monomial_r);
            }
        }
        return compression(result);
    }

    public static void divideByMonom(Polynomial Q,Polynomial p1, Polynomial p2)
    {
        if(p2.getMonom(0).getCoefficient()==0)
            JOptionPane.showMessageDialog(null,"Division by 0!!!","Fatal error",JOptionPane.ERROR_MESSAGE);
        else
        for(Monomial m: p1.myPolynomial)
        {
            Q.addToPolynomial(new Monomial(m.coefficient/p2.getMonom(0).getCoefficient(),m.getExponent()-p2.getMonom(0).getExponent()));
        }
        p1.clearPolynomial();
    }

    public static String division(Polynomial p1, Polynomial p2) {

        Polynomial Q = new Polynomial(0, 0);
        Polynomial R = new Polynomial(0,0);
        p1.sortPolynomial();
        if (p2.termNumber == 1) Operations.divideByMonom(Q,p1, p2);
        else {
            if (p2.degree > p1.degree) {
                String result;
                StringBuilder bd = new StringBuilder();
                result = bd.append("Q:0; R:").toString();
                for (Monomial m : p1.myPolynomial)
                    if (m.getCoefficient() > 0)
                        result = bd.append("+" + m.getCoefficient() + "x^" + m.getExponent()).toString();
                    else result = bd.append(m.getCoefficient() + "x^" + m.getExponent()).toString();
                return result;
            } else {
                while (p1.degree >= p2.degree && !p1.noMonom()) {
                    Monomial monomial_aux = new Monomial(p1.getMonom(0).getCoefficient() / p2.getMonom(0).getCoefficient(), p1.degree - p2.degree);
                    Polynomial polynomial_aux = new Polynomial(1, p1.degree - p2.degree);
                    polynomial_aux.myPolynomial.add(monomial_aux);
                    Q.addToPolynomial(monomial_aux);
                    Polynomial polynomial_resultm = multiply(polynomial_aux, p2);
                    p1 = subtract(p1, polynomial_resultm);
                    polynomial_aux.clearPolynomial();
                }
            }
        }
        return Operations.result(Q, R, p1, p2);
    }

    public static String result(Polynomial Q, Polynomial R, Polynomial p1, Polynomial p2)
    {
        if(p1.noMonom())R.clearPolynomial();
        String q,r;
        StringBuilder sbq = new StringBuilder();
        StringBuilder sbr = new StringBuilder();
        q = sbq.append("Q:").toString();
        r = sbr.append("R:").toString();
        for(Monomial m: Q.myPolynomial) {
            if(m.getCoefficient()>0)
                q = sbq.append("+"+m.getCoefficient() + "x^" + m.getExponent()).toString();
            else q = sbq.append(m.getCoefficient() + "x^" + m.getExponent()).toString();
        }
        if(!R.noMonom())
        {
            for(Monomial m: p1.myPolynomial)
                if(m.getCoefficient()>0)
                    r = sbq.append("+"+m.getCoefficient() + "x^" + m.getExponent()).toString();
                else r = sbq.append(m.getCoefficient() + "x^" + m.getExponent()).toString();
        }
        else r = sbr.append("0").toString();
        return q + "; " + r;

    }

    public static Polynomial derivative(Polynomial p)
    {
        Polynomial result = new Polynomial(p.termNumber,p.degree);
        for(Monomial m: p.myPolynomial)
        {
            Monomial monomial_derivative = new Monomial(m.getCoefficient()*m.getExponent(),m.getExponent()-1);
            result.addToPolynomial(monomial_derivative);
        }
        result.sortPolynomial();
        return result;
    }

    public static Polynomial integrate(Polynomial p)
    {
        Polynomial result = new Polynomial(p.termNumber,p.degree);
        for(Monomial m: p.myPolynomial)
        {
            double div; div = Math.pow((double)m.getExponent()+1,-1);
            double coeff = m.getCoefficient() * div;
            Monomial monomial_integrate = new Monomial(coeff,m.getExponent()+1);
            result.addToPolynomial(monomial_integrate);
        }
        result.sortPolynomial();
        return result;
    }

}
