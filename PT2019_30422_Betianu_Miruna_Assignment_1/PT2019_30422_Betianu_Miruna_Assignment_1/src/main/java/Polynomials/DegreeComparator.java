package Polynomials;
import java.util.Comparator;

public class DegreeComparator implements Comparator<Monomial>{
    public int compare(Monomial o1, Monomial o2) {
        if(o1.getExponent() != o2.getExponent())
        return (o2.getExponent() - o1.getExponent());
        else return -1;
    }
}
