package Polynomials;

public class Monomial {

    public double coefficient;
    public Integer exponent;

    public void setCoefficient(double coefficient)
    {
        this.coefficient = coefficient;
    }

    public double getCoefficient()
    {
        return this.coefficient;
    }

    public void setExponent(Integer exponent)
    {
        this.exponent = exponent;
    }

    public Integer getExponent()
    {
        return this.exponent;
    }

    public Monomial(double coefficient, Integer exponent)
    {
        this.setCoefficient(coefficient);
        this.setExponent(exponent);

    }
}
