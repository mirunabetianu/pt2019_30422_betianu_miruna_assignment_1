package GUI;

import Polynomials.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {

    private View view;
    private Model model;

    Controller (Model model, View view)
    {
        this.model = model;
        this.view = view;

        view.addSumListener(new addSumListener());
        view.addSubtractListener(new addSubtractListener());
        view.addMultiplyListener(new addMultiplyListener());
        view.addDivisionListener(new addDivisionListener());
        view.addDerivativeListener(new addDerivativeListener());
        view.addIntegrateListener(new addIntegrateListener());
    }

    public class addSumListener implements ActionListener
    {

        public void actionPerformed(ActionEvent e) {

                String userInput1 = "";
                String userInput2 = "";
                try {

                    userInput1 = view.getInput1();
                    userInput2 = view.getInput2();

                    Polynomial P,Q;

                    P = Model.toPolynomial(userInput1);
                    Q = Model.toPolynomial(userInput2);

                    Polynomial result = Operations.sum(P, Q);

                    String final_result = Model.toString(result);

                    view.setResult(final_result);


                } catch (NumberFormatException nfex) {
                    view.showError("Bad input: '" + userInput1 + "'" + userInput2);
                }
        }
    }

    public class addSubtractListener implements ActionListener
    {

        public void actionPerformed(ActionEvent e) {

            String userInput1 = "";
            String userInput2 = "";
            try {

                userInput1 = view.getInput1();
                userInput2 = view.getInput2();

                Polynomial P,Q;

                P = Model.toPolynomial(userInput1);
                Q = Model.toPolynomial(userInput2);

                Polynomial result = Operations.subtract(P, Q);

                String final_result = Model.toString(result);

                view.setResult(final_result);


            } catch (NumberFormatException nfex) {
                view.showError("Bad input: '" + userInput1 + "'" + userInput2);
            }
        }
    }

    public class addMultiplyListener implements ActionListener
    {

        public void actionPerformed(ActionEvent e) {

            String userInput1 = "";
            String userInput2 = "";
            try {

                userInput1 = view.getInput1();
                userInput2 = view.getInput2();

                Polynomial P,Q;

                P = Model.toPolynomial(userInput1);
                Q = Model.toPolynomial(userInput2);

                Polynomial result = Operations.multiply(P, Q);

                String final_result = Model.toString(result);

                view.setResult(final_result);


            } catch (IllegalArgumentException e1) {
                view.showError("Bad input: '" + userInput1 + "'" + userInput2);
            }
        }
    }

    public class addDivisionListener implements ActionListener
    {

        public void actionPerformed(ActionEvent e) {

            String userInput1 = "";
            String userInput2 = "";
            try {

                userInput1 = view.getInput1();
                userInput2 = view.getInput2();

                Polynomial P,Q;

                P = Model.toPolynomial(userInput1);
                Q = Model.toPolynomial(userInput2);

                String final_result = Operations.division(P, Q);

                view.setResult(final_result);

            } catch (IllegalArgumentException e1) {
                view.showError("Bad input: '" + userInput1 + "'" + userInput2);
            }
        }
    }

    public class addIntegrateListener implements ActionListener
    {

        public void actionPerformed(ActionEvent e) {

            String userInput = "";
            try {

                userInput = view.getInput1();

                Polynomial P;

                P = Model.toPolynomial(userInput);

                Polynomial result = Operations.integrate(P);

                String final_result = Model.toString(result);

                view.setResult(final_result);


            } catch (IllegalArgumentException e1) {
                view.showError("Bad input: '" + userInput);
            }
        }
    }

    public class addDerivativeListener implements ActionListener
    {

        public void actionPerformed(ActionEvent e) {

            String userInput = "";
            try {

                userInput = view.getInput1();

                Polynomial P;

                P = Model.toPolynomial(userInput);

                Polynomial result = Operations.derivative(P);

                String final_result = Model.toString(result);

                view.setResult(final_result);


            } catch (IllegalArgumentException e1) {
                view.showError("Bad input: '" + userInput);
            }
        }
    }

}
