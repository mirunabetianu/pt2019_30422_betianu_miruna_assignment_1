package GUI;

import java.awt.*;
import java.awt.event.ActionListener;
import javax.swing.*;

class View extends JFrame{

    private JTextField textPoly1 = new JTextField(40);
    private JTextField textPoly2 = new JTextField(40);
    private JTextField textResult = new JTextField(40);

    private JButton sumButton = new JButton("Sum");
    private JButton subtractButton = new JButton("Subtract");
    private JButton multiplyButton = new JButton("Multiply");
    private JButton divideButton = new JButton("Divide");
    private JButton derivativeButton = new JButton("(     )'");
    private JButton integrateButton = new JButton("∫﹙﹚dx");



    View(Model model) {
        JPanel p1 = new JPanel();
        JPanel p2 = new JPanel();
        JPanel p3 = new JPanel();
        JPanel p_aux = new JPanel();
        JPanel other_panel = new JPanel();
        JPanel p4 = new JPanel();

        p1.add(new JLabel("1st Polynomial "));
        p1.add(textPoly1);
        p_aux.add(derivativeButton);
        p_aux.add(integrateButton);
        JLabel x = new JLabel("The polynomial MUST be introduced following the pattern: ax^n±bx^m± ... ±cx^p!");
        x.setForeground(Color.decode("#ff6200"));
        p4.add(x);

        other_panel.setLayout(new BoxLayout(other_panel,BoxLayout.Y_AXIS));
        other_panel.add(p4);
        other_panel.add(p1);
        other_panel.add(p_aux);

        p2.add(new JLabel("2nd Polynomial"));
        p2.add(textPoly2);

        p3.add(new JLabel("  Result            "));
        p3.add(textResult);
        textResult.setEditable(false);


        p1.setLayout(new FlowLayout());
        p2.setLayout(new FlowLayout());
        p3.setLayout(new FlowLayout());

        JPanel panel = new JPanel();
        panel.add(other_panel);
        panel.add(p2);
        panel.add(p3);

        JPanel buttons = new JPanel();
        buttons.add(sumButton);
        buttons.add(subtractButton);
        buttons.add(multiplyButton);
        buttons.add(divideButton);

        panel.setLayout(new BoxLayout(panel,BoxLayout.Y_AXIS));
        JPanel aux = new JPanel();
        aux.add(panel);
        aux.add(buttons);

        JFrame mainFrame = new JFrame();

        aux.setLayout(new BoxLayout(aux,BoxLayout.Y_AXIS));

        mainFrame.add(aux);
        mainFrame.pack();
        mainFrame.setVisible(true);
        mainFrame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setTitle("Polynomial Calculator");

    }
    String getInput1(){ return textPoly1.getText();}
    String getInput2(){ return textPoly2.getText();}
    void setResult(String text){ textResult.setText(text);}
    void showError(String errMessage) {
        JOptionPane.showMessageDialog(this, errMessage);
    }
    void addSumListener(ActionListener sum) {
        sumButton.addActionListener(sum);
    }
    void addSubtractListener(ActionListener sub) {
        subtractButton.addActionListener(sub);
    }
    void addMultiplyListener(ActionListener mul) {
        multiplyButton.addActionListener(mul);
    }
    void addDivisionListener(ActionListener div) {
        divideButton.addActionListener(div);
    }
    void addDerivativeListener(ActionListener der) {
        derivativeButton.addActionListener(der);
    }
    void addIntegrateListener(ActionListener intgr) {
        integrateButton.addActionListener(intgr);
    }
}
