package GUI;
import Polynomials.*;

import javax.swing.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Model {

    static Polynomial toPolynomial(String text)
    {
        Polynomial polynomial = new Polynomial(0,0);
        try {
            Pattern p = Pattern.compile( "(-?\\b\\d+)[xX]\\^(-?\\d+\\b)" );
            Matcher m = p.matcher( text );
            if(m.find()) {
                do{
                    Monomial monomial = new Monomial(Double.parseDouble(m.group(1)), Integer.parseInt(m.group(2)));
                    polynomial.addToPolynomial(monomial);
                }while(m.find());
            } else throw new IllegalArgumentException("Not respecting the format");
        }
        catch(IllegalArgumentException e)
        {
            JOptionPane.showMessageDialog(null,"The input does not respect the pattern!","Error",JOptionPane.ERROR_MESSAGE);
        }
        return polynomial;
    }

    static String toString(Polynomial p)
    {
        String polynomial="";
        StringBuilder sb = new StringBuilder();

        for(Monomial m: p.myPolynomial) {
            if (m.getExponent() != 0) {
                if (m.getCoefficient() > 0) {
                    if (m.getExponent() == 1 && m.getCoefficient() == 1) polynomial = sb.append("+x").toString();
                    else if (m.getCoefficient() == 1) polynomial = sb.append("+x^" + m.getExponent()).toString();
                    else if (m.getExponent() == 1) polynomial = sb.append("+" + m.getCoefficient() + "x").toString();
                    else polynomial = sb.append("+" + m.getCoefficient() + "x^" + m.getExponent()).toString();
                } else if (m.getCoefficient() == -1) polynomial = sb.append("-" + "x^" + m.getExponent()).toString();
                else polynomial = sb.append(m.getCoefficient() + "x^" + m.getExponent()).toString();
            }
            else
                if(m.getCoefficient()>0) polynomial = sb.append("+"+m.getCoefficient()).toString();
                else polynomial = sb.append(m.getCoefficient()).toString();
        }

        return polynomial;
    }


}
